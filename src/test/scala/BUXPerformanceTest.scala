
import com.atlassian.bux.{AIDLoginHelper, ConfigHelper}
import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._
import scala.util.Random

class BUXPerformanceTest extends Simulation {

  ConfigHelper.initialise()

  val cookie = new AIDLoginHelper().getCookie()
  val instances = ConfigHelper.instances()

  def pickARandomInstance() = {
    val instance = instances.get(Random.nextInt(instances.size()))
    Map("instance" -> instance)
  }

  val instancesFeeder = Iterator.continually(pickARandomInstance)

  val httpProtocol = http
    .contentTypeHeader("application/json")

  val headers = Map(
    "Referer" -> "${instance}",
    "Cookie" -> cookie
  )

  val billEstimateScenario = scenario("BUX Bill Estimate")
    .feed(instancesFeeder)
    .exec(
      http("BUX_bill-estimate")
        .get("${instance}/admin/rest/billing-ux/api/billing/bill-estimate")
        .headers(headers))

  val billingDetailsScenario = scenario("BUX Billing Details")
    .feed(instancesFeeder)
    .exec(
      http("BUX_bill-details")
        .get("${instance}/admin/rest/billing-ux/api/billing/billing-details")
        .headers(headers))

  setUp(
    billEstimateScenario.inject(rampUsersPerSec(1) to (ConfigHelper.totalUsers()) during (ConfigHelper.rampUpTimeInSec() seconds),
      constantUsersPerSec(ConfigHelper.totalUsers()) during (ConfigHelper.runTimeInSec() seconds)),

    billingDetailsScenario.inject(rampUsersPerSec(1) to (ConfigHelper.totalUsers()) during (ConfigHelper.rampUpTimeInSec() seconds),
      constantUsersPerSec(ConfigHelper.totalUsers()) during (ConfigHelper.runTimeInSec() seconds))
  ).protocols(httpProtocol)
}